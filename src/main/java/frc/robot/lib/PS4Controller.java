package frc.robot.lib;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

public class PS4Controller extends Joystick{
    
    public PS4Controller(int port) {
        super(port);

    }

    public enum Button{
        Square(1),
        X(2),
        Circle(3),
        Triangle(4),
        BumperLeft(5),
        BumperRight(6),
        TriggerLeft(7),
        TriggerRight(8),
        Share(9),
        Options(10),
        StickLeft(11),
        StickRight(12),
        Playstation(13),
        Touchpad(14);

        private Button(int value){
            this.m_value = value;
        }
        private int m_value;
    }

    public enum Axis{
        LeftY(1),
        RightY(5),
        LeftX(0),
        RightX(2),
        LeftTrigger(3),
        RightTrigger(4);

        private int m_value;

        private Axis(int value){
            this.m_value = value;
        }
        
    }

	public JoystickButton JoystickButton(Button button) {
        return new JoystickButton(this, button.m_value);
	}


}
